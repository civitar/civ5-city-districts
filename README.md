# CITY DISTRICTS

Allows cities to expand across multiple tiles, growing and specializing to a far greater degree. Each city can build one of each kind of district, and each district provides heavy bonuses in a particular direction.
Districts appear when a player constructs a special building in the city. When the building is complete, the corresponding tile improvement appears next to the city. Capital cities can build up to 6 districts, while normal cities are limited to 3 districts.
The current district types and their focuses are:
- Trade District (gold, trade routes)
- Residential District (food)
- Art District (culture, tourism, science)
- School District (science, gold, happiness)
- Warehouse District (production, food)
- Temple District (faith, culture)
- Entertainment District (happiness, culture, tourism)
- Docks District (naval production, gold, food, trade routes)
- Military District (military production, defense)

Eventually we may have districts be created by great people instead of great person tile improvements, in which case the associations would be as follows:
- Trade District/Great Merchant
- Art District/Great Artist
- School District/Great Scientist
- Warehouse District/Great Engineer
- Temple District/Great Prophet
- Entertainment District/Great Musician
- Docks District/Great Admiral
- Military District/Great General